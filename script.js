fetch('https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json').then(response => response.json())
.then(dati => {

    //formattazione data
    let sorted = dati.reverse()
    let lastUpdated = sorted[0].data
    let lastUpdatedFormatted = lastUpdated.split("T")[0].split("-").reverse().join("/")
    document.querySelector("#date").innerHTML = lastUpdatedFormatted
   
    //ultimo giorno di tutte le regioni
    let lastUpdatedData = sorted.filter(el => el.data == lastUpdated).sort((a, b) => b.nuovi_positivi - a.nuovi_positivi)

    //modali
    let modal = document.querySelector('.modal-custom')
    let modalContent = document.querySelector('.modal-custom-content')

 

    //casi
    let totalCases = lastUpdatedData.map(el => el.totale_casi).reduce((t, n) => t + n)
    document.querySelector("#totalCases").innerHTML = totalCases
                 

    let totalRecovered = lastUpdatedData.map(el => el.dimessi_guariti).reduce((t, n) => t + n)
    document.querySelector("#totalRecovered").innerHTML = totalRecovered

    let totalDeath = lastUpdatedData.map(el => el.deceduti).reduce((t, n) => t + n)
    document.querySelector("#totalDeath").innerHTML = totalDeath

    let totalPositive = lastUpdatedData.map(el => el.totale_positivi).reduce((t, n) => t + n)
    document.querySelector("#totalPositive").innerHTML = totalPositive

    let totalTerapia = lastUpdatedData.map(el => el.terapia_intensiva).reduce((t, n) => t + n)
    document.querySelector("#totalTerapia").innerHTML = totalTerapia

    let totalIsolamento = lastUpdatedData.map(el => el.isolamento_domiciliare).reduce((t, n) => t + n)
    document.querySelector("#totalIsolamento").innerHTML = totalIsolamento

    
    let days = Array.from(new Set(sorted.map(el => el.data))).reverse()
    
    let totalsPerDays = days.map(el => [el, sorted.filter(i => i.data == el).map(e => e.totale_casi).reduce((t, n) => t + n)])
    let MaxDefault = Math.max(...totalsPerDays.map(el => el[1]))
    
    let defaultGraphic = document.querySelector('#defaultGraphic').innerHTML = `
                <div class="container mx-0 px-0">
                    <div class="row">
                        <div class="col-10 bg-card mx-0 px-0">
                         <p class="h3 m-3">TOTALE CASI</p>
                            <div id="standardTrend" class=" d-flex align-items-end" style="height: 250px;"></div>
                        </div>
                    </div>
                </div>
                 `

                 let standardTrend = document.querySelector('#standardTrend')
                 

                 totalsPerDays.forEach(el => {

                     let colStandard = document.createElement('div')
                     colStandard.classList.add('d-inline-block', 'bg-primary')
                     colStandard.style.width = "15px"
                     colStandard.style.marginRight = "1px"
                     colStandard.style.height = `${70 * el[1]/MaxDefault}%`

                     standardTrend.appendChild(colStandard)

                 })

                 //effetto active sulle card
                let btnCustom = document.querySelector('.card-custom')
                btnCustom.addEventListener('click', () => {
                     btnCustom.classList.toggle('activeCard')
                     btnCustom1.classList.remove('activeCard')
                     btnCustom2.classList.remove('activeCard')
                     btnCustom3.classList.remove('activeCard')
                     btnCustom4.classList.remove('activeCard')
                     btnCustom5.classList.remove('activeCard')
                 }) 
                 let btnCustom1 = document.querySelector('.card-custom1')
                btnCustom1.addEventListener('click', () => {
                     btnCustom1.classList.toggle('activeCard')
                     btnCustom.classList.remove('activeCard')
                     btnCustom2.classList.remove('activeCard')
                     btnCustom3.classList.remove('activeCard')
                     btnCustom4.classList.remove('activeCard')
                     btnCustom5.classList.remove('activeCard')
                 }) 
                 let btnCustom2 = document.querySelector('.card-custom2')
                btnCustom2.addEventListener('click', () => {
                     btnCustom2.classList.toggle('activeCard')
                     btnCustom.classList.remove('activeCard')
                     btnCustom1.classList.remove('activeCard')
                     btnCustom3.classList.remove('activeCard')
                     btnCustom4.classList.remove('activeCard')
                     btnCustom5.classList.remove('activeCard')
                 }) 
                 let btnCustom3 = document.querySelector('.card-custom3')
                btnCustom3.addEventListener('click', () => {
                     btnCustom3.classList.toggle('activeCard')
                     btnCustom.classList.remove('activeCard')
                     btnCustom1.classList.remove('activeCard')
                     btnCustom2.classList.remove('activeCard')
                     btnCustom4.classList.remove('activeCard')
                     btnCustom5.classList.remove('activeCard')
                 }) 
                 let btnCustom4 = document.querySelector('.card-custom4')
                btnCustom4.addEventListener('click', () => {
                     btnCustom4.classList.toggle('activeCard')
                     btnCustom.classList.remove('activeCard')
                     btnCustom1.classList.remove('activeCard')
                     btnCustom2.classList.remove('activeCard')
                     btnCustom3.classList.remove('activeCard')
                     btnCustom5.classList.remove('activeCard')
                 }) 
                 let btnCustom5 = document.querySelector('.card-custom5')
                btnCustom5.addEventListener('click', () => {
                     btnCustom5.classList.toggle('activeCard')
                     btnCustom.classList.remove('activeCard')
                     btnCustom1.classList.remove('activeCard')
                     btnCustom2.classList.remove('activeCard')
                     btnCustom3.classList.remove('activeCard')
                     btnCustom4.classList.remove('activeCard')
                 }) 
                
    

        document.querySelectorAll('[data-trend').forEach(el => {
        
            el.addEventListener('click', () => {

                let set = el.dataset.trend

                
                let totalsPerDays = days.map(el => [el, sorted.filter(i => i.data == el).map(e => e[set]).reduce((t, n) => t + n)])
                let maxData = Math.max(...totalsPerDays.map(el => el[1]))
                
                
                let graphicWrapper = document.querySelector('.graphicWrapper')
                
                
                graphicWrapper.innerHTML =
                `
                <div class="container mx-0 px-0">
                    <div class="row">
                        <div class="col-10 bg-card mx-0 px-0">
                        <p class="h3  m-3">${set.replace(/_/g, " ").replace(/dimessi/g, "").toUpperCase()}</p>
                            <div id="totalTrend" class="  d-flex align-items-end" style="height: 250px;"></div>
                        </div>
                    </div>
                </div>
                 `

                 let totalTrend = document.querySelector('#totalTrend')
                 

                 totalsPerDays.forEach(el => {

                     let col = document.createElement('div')
                     col.classList.add('d-inline-block', 'bg-graphic')
                     col.style.width = "15px"
                     col.style.marginRight = "1px"
                     col.style.height = `${70 * el[1] / maxData}%`

                     totalTrend.appendChild(col)

                 })
            })


            
        })






    //card
    let CardWrapper = document.querySelector("#cardWrapper")

    lastUpdatedData.forEach(el => {
        let div = document.createElement('div')
        div.classList.add('col-12', 'col-lg-6', 'my-4')
        div.innerHTML =
            `

     <div class="card-custom  h-100" data-region="${el.denominazione_regione}">
        <p class="m-1">${el.denominazione_regione}</p>
        <p class="text-end mb-1">${el.nuovi_positivi}</p>
     </div>
    
`
        CardWrapper.appendChild(div)
    });


    //progress Bar
    let progressWrapper = document.querySelector("#progressWrapper")

    let todayMax = Math.max(...lastUpdatedData.map(el => el.nuovi_positivi))

    lastUpdatedData.forEach(el => {
        let div = document.createElement('div')
        div.classList.add('col-12', 'mb-4')
        div.innerHTML =
            `

<div class="col-12 mb-4">
        <p class="mb-0">${el.denominazione_regione} : ${el.nuovi_positivi}</p>
        <div class="progress rounded-0">
            <div class="progress-bar" style="width: ${100 * el.nuovi_positivi / todayMax}%;">
            </div>
        </div>
    </div>
    
`

        progressWrapper.appendChild(div)
    });




    document.querySelectorAll('[data-region').forEach(el => {
        el.addEventListener('click', () => {

            let region = el.dataset.region
            modal.classList.add('active')

            let dataAboutRegion = lastUpdatedData.filter(el => el.denominazione_regione == region)[0]
            modalContent.innerHTML =
                `
<div class="container">
    <div class="row">
        <div class="col-12">
            <p class="h2 my-2">${region}</p>
        </div>
        <div class="col-12 col-lg-6 my-3">
            <p class="lead">Totale Casi: ${dataAboutRegion.totale_casi}</p>
            <p class="lead">Nuovi Positivi: ${dataAboutRegion.nuovi_positivi}</p>
            <p class="lead">Deceduti: ${dataAboutRegion.deceduti}</p>
            <p class="lead">Guariti: ${dataAboutRegion.dimessi_guariti}</p>
            <p class="lead">Ricoverati con sintomi: ${dataAboutRegion.ricoverati_con_sintomi}</p>
            
        </div>
        <div class="col-12 col-lg-6 my-3">
            <p class="lead">Eventuali Note: </p>
            <p class="lead">${(dataAboutRegion.note == null) ? ("Nessuna nota da segnalare") : dataAboutRegion.note} </p>
        </div>
        <div class="col-12 d-none d-md-block">
            <p class="mt-3">Trend Nuovi Casi</p>
            <div id="trendNew" class="d-flex align-items-end border border-primary" style="height: 150px;"></div>
        </div>
        <div class="col-12 d-none d-md-block">
            <p class="mt-3">Trend Decessi</p>
            <div id="trendDeath" class="d-flex align-items-end border border-danger" style="height: 150px;"></div>
        </div>
        <div class="col-12 d-none d-md-block">
            <p class="mt-3">Trend Guariti</p>
            <div id="trendRecovered" class="d-flex align-items-end border border-success" style="height: 150px;"></div>
            <p class="mt-3 small">Covid Dashboard</p>
        </div>
    </div>
</div>

`
            let trendData = sorted.map(el => el).reverse().filter(el => el.denominazione_regione == region).map(el => [el.data, el.nuovi_positivi, el.deceduti, el.dimessi_guariti])

         
            
            let trendNew = document.querySelector('#trendNew')
            let trendDeath = document.querySelector('#trendDeath')
            let trendRecovered = document.querySelector('#trendRecovered')

            let maxNew = Math.max(...trendData.map(el => el[1]))
            let maxDeath = Math.max(...trendData.map(el => el[2]))
            let maxRecovered = Math.max(...trendData.map(el => el[3]))

          

            trendData.forEach(el => {

                let colNew = document.createElement('div')
                colNew.classList.add('d-inline-block', 'bg-primary')
                colNew.style.width = "15px"
                colNew.style.marginRight = "1px"
                colNew.style.height = `${70 * el[1] / maxNew}%`

                trendNew.appendChild(colNew)


                let colDeath = document.createElement('div')
                colDeath.classList.add('d-inline-block', 'bg-danger')
                colDeath.style.width = "15px"
                colDeath.style.marginRight = "1px"
                colDeath.style.height = `${70 * el[2] / maxDeath}%`

                trendDeath.appendChild(colDeath)

                let colRecovered = document.createElement('div')
                colRecovered.classList.add('d-inline-block', 'bg-success')
                colRecovered.style.width = "15px"
                colRecovered.style.marginRight = "1px"
                colRecovered.style.height = `${70 * el[3] / maxRecovered}%`

                trendRecovered.appendChild(colRecovered)
            })


        })

        






        window.addEventListener('click', function (e) {
            if (e.target == modal) {
                modal.classList.remove('active')
            }
        })
    })


})




//console log system
var consoleLogo = ` 

                                ████████████████████████████████████████████████████████████████████████
                                █░░░░░░░░░░░░░░█░░░░░░░░░░░░░░█░░░░░░██░░░░░░█░░░░░░░░░░█░░░░░░░░░░░░███
                                █░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀░░░░█
                                █░░▄▀░░░░░░░░░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░██░░▄▀░░█░░░░▄▀░░░░█░░▄▀░░░░▄▀▄▀░░█
                                █░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░███░░▄▀░░███░░▄▀░░██░░▄▀░░█
                                ████████████████████████████████████████████████████████████████████████
                                █░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░███░░▄▀░░███░░▄▀░░██░░▄▀░░█
                                █░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░███░░▄▀░░███░░▄▀░░██░░▄▀░░█
                                █░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░███░░▄▀░░███░░▄▀░░██░░▄▀░░█
                                █░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀▄▀░░▄▀▄▀░░███░░▄▀░░███░░▄▀░░██░░▄▀░░█
                                █░░▄▀░░░░░░░░░░█░░▄▀░░░░░░▄▀░░█░░░░▄▀▄▀▄▀░░░░█░░░░▄▀░░░░█░░▄▀░░░░▄▀▄▀░░█
                                █░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░███░░░░▄▀░░░░███░░▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀░░░░█
                                █░░░░░░░░░░░░░░█░░░░░░░░░░░░░░█████░░░░░░█████░░░░░░░░░░█░░░░░░░░░░░░███
██████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████
█░░░░░░░░░░░░███░░░░░░░░░░░░░░█░░░░░░░░░░░░░░█░░░░░░██░░░░░░█░░░░░░░░░░░░░░███░░░░░░░░░░░░░░█░░░░░░░░░░░░░░█░░░░░░░░░░░░░░░░███░░░░░░░░░░░░███
█░░▄▀▄▀▄▀▄▀░░░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░███░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀▄▀░░███░░▄▀▄▀▄▀▄▀░░░░█
█░░▄▀░░░░▄▀▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░░░░░█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░▄▀░░███░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░░░▄▀░░███░░▄▀░░░░▄▀▄▀░░█
█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░███░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░████░░▄▀░░███░░▄▀░░██░░▄▀░░█
█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░░░░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░▄▀░░░░█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░░░▄▀░░███░░▄▀░░██░░▄▀░░█
█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀▄▀░░███░░▄▀░░██░░▄▀░░█
█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░░░░░░░░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░▄▀░░░░███░░▄▀░░██░░▄▀░░█
█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░█████████░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░████░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░█████░░▄▀░░██░░▄▀░░█
█░░▄▀░░░░▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░░░░░░░░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░░░░░█░░▄▀░░░░▄▀▄▀░░█
█░░▄▀▄▀▄▀▄▀░░░░█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀░░░░█
█░░░░░░░░░░░░███░░░░░░██░░░░░░█░░░░░░░░░░░░░░█░░░░░░██░░░░░░█░░░░░░░░░░░░░░░░█░░░░░░░░░░░░░░█░░░░░░██░░░░░░█░░░░░░██░░░░░░░░░░█░░░░░░░░░░░░███
██████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████_|
#                                                                                `
console.log(consoleLogo)