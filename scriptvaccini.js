fetch('https://raw.githubusercontent.com/italia/covid19-opendata-vaccini/master/dati/anagrafica-vaccini-summary-latest.json').then(response => response.json())
.then(dati => {

let array = dati.data

//aggiornamento data
let lastUpdated = array[0].ultimo_aggiornamento
let date = lastUpdated.split("T")[0].split("-").reverse().join("/")
document.querySelector('#date').innerHTML=date

//vaccinati alla prima dose
let prima_dose = array.map(el => el.prima_dose).reduce((t,n) => t + n)
document.querySelector('#primaDose').innerHTML= new Intl.NumberFormat().format(prima_dose)

//vaccinati alla seconda dose
let seconda_dose = array.map(el => el.seconda_dose).reduce((t,n) => t + n)
document.querySelector('#secondaDose').innerHTML= new Intl.NumberFormat().format(seconda_dose)

//grafico
var oilCanvas = document.getElementById("oilChart");
Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 25;

var oilData = {
    labels: [
        "Popolazione Italia",
        "Prima Dose",
        "Vaccinati"
    ],
    datasets: [
        {
            data: [59260000, prima_dose , seconda_dose],
            backgroundColor: [
                "#33aeff",
                "#d4dcdf",
                "#afadb5"
                
            ]
        }]
};

var pieChart = new Chart(oilCanvas, {
  type: 'pie',
  data: oilData
});

})

//somministrazioni vaccini
fetch('https://raw.githubusercontent.com/italia/covid19-opendata-vaccini/master/dati/somministrazioni-vaccini-summary-latest.json').then(response => response.json())
.then(dati => {
    let array = dati.data
    //ultimo giorno di tutte le regioni
    let a = array.map(el => el.data_somministrazione.split('T')[0].split("-").join(''))
    let lastUpdated = a.sort(function(a, b){return b-a});
    let lastUpdatedData = array.filter(el => el.data_somministrazione.split('T')[0].split("-").join('') == lastUpdated[0])

    let VacciniOggi = lastUpdatedData.sort((a, b) => b.totale - a.totale)
    VacciniOggi = lastUpdatedData.map(el => el.totale).reduce((t,n) => t +n)
    document.querySelector("#totals").innerHTML= VacciniOggi

let CardWrapper = document.querySelector("#cardWrapper")

lastUpdatedData.forEach(el => {
    let div = document.createElement('div')
    div.classList.add('col-12', 'col-lg-3', 'my-4')
    div.innerHTML =
        `
 <div class="card-custom  h-100" data-region="${el.nome_area}">
    <p>${el.nome_area}</p>
    <p class="text-start">Vaccinati oggi <span class="float-end">${new Intl.NumberFormat().format(el.totale)}</span></p>
 </div>

`

CardWrapper.appendChild(div)

});



    //modali
    let modal = document.querySelector('.modal-custom')
    let modalContent = document.querySelector('.modal-custom-content')


document.querySelectorAll('[data-region').forEach(el => {
    el.addEventListener('click', () => {

        let region = el.dataset.region
        modal.classList.add('active')
        
        let totalsForRegion =array.filter(el => el.nome_area == region).map(el => el.totale).reduce((t,n) => t + n)
        

        let dataAboutRegion = lastUpdatedData.filter(el => el.nome_area == region)[0]
        modalContent.innerHTML =
`
<div class="container">
    <div class="row">
        <div class="col-12">
            <p class="h2 my-2">${region}</p>
        </div>
        <div class="col-12 col-lg-6 my-3">
            <p class="lead">Totale vaccinati: ${new Intl.NumberFormat().format(totalsForRegion)}</p>
            <p class="lead">Vaccinati oggi prima dose: ${new Intl.NumberFormat().format(dataAboutRegion.prima_dose)}</p>
            <p class="lead">Vaccinati oggi seconda dose: ${new Intl.NumberFormat().format(dataAboutRegion.seconda_dose)}
            </p>
        </div>
        <div class="col-12 col-lg-6 my-3">

        </div>
        <div class="col-12 d-none d-md-block">
            <p class="mt-3">Trend Vaccini giornalieri</p>
            <div id="vacciniTrend" class="d-flex align-items-end border border-primary" style="height: 150px;"></div>
        </div>
        <div class="col-12 d-none d-md-block mt-4">
            <p class="mt-3">Vaccinati oggi prima dose</p>
            <div id="vacciniPrimaTrend" class="d-flex align-items-end border border-info" style="height: 150px;"></div>
        </div>
        <div class="col-12 d-none d-md-block mt-4">
            <p class="mt-3">Vaccinati oggi seconda dose</p>
            <div id="vacciniSecondaTrend" class="d-flex align-items-end border border-secondary" style="height: 150px;"></div>
        </div>
    </div>
</div>
`

let vacciniTrend = document.querySelector('#vacciniTrend')
let vacciniPrimaTrend = document.querySelector('#vacciniPrimaTrend')
let vacciniSecondaTrend = document.querySelector('#vacciniSecondaTrend')
let trendData = array.map(el => el).filter(el => el.nome_area == region).map(el => [el.totale, el.prima_dose, el.seconda_dose])
let maxNew = Math.max(...trendData.map(el => el[0]))
let maxPrima = Math.max(...trendData.map(el => el[1]))
let maxSecondo = Math.max(...trendData.map(el => el[2]))

trendData.forEach(el => {

    let colNew = document.createElement('div')
    colNew.classList.add('d-inline-block', 'bg-primary')
    colNew.style.width = "6px"
    colNew.style.marginRight = "2px"
    colNew.style.height = `${70 * el[0] / maxNew}%`

    vacciniTrend.appendChild(colNew)

    let colPrima= document.createElement('div')
    colPrima.classList.add('d-inline-block', 'bg-info')
    colPrima.style.width = "6px"
    colPrima.style.marginRight = "2px"
    colPrima.style.height = `${70 * el[1] / maxPrima}%`

    vacciniPrimaTrend.appendChild(colPrima)

    let colSecondo= document.createElement('div')
    colSecondo.classList.add('d-inline-block', 'bg-secondary')
    colSecondo.style.width = "6px"
    colSecondo.style.marginRight = "2px"
    colSecondo.style.height = `${70 * el[2] / maxSecondo}%`

    vacciniSecondaTrend.appendChild(colSecondo)



})
        

})

    window.addEventListener('click', function (e) {
        if (e.target == modal) {
            modal.classList.remove('active')
        }
    })
})



})


//console log system
var consoleLogo = ` 

                                ████████████████████████████████████████████████████████████████████████
                                █░░░░░░░░░░░░░░█░░░░░░░░░░░░░░█░░░░░░██░░░░░░█░░░░░░░░░░█░░░░░░░░░░░░███
                                █░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀░░░░█
                                █░░▄▀░░░░░░░░░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░██░░▄▀░░█░░░░▄▀░░░░█░░▄▀░░░░▄▀▄▀░░█
                                █░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░███░░▄▀░░███░░▄▀░░██░░▄▀░░█
                                ████████████████████████████████████████████████████████████████████████
                                █░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░███░░▄▀░░███░░▄▀░░██░░▄▀░░█
                                █░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░███░░▄▀░░███░░▄▀░░██░░▄▀░░█
                                █░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░███░░▄▀░░███░░▄▀░░██░░▄▀░░█
                                █░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀▄▀░░▄▀▄▀░░███░░▄▀░░███░░▄▀░░██░░▄▀░░█
                                █░░▄▀░░░░░░░░░░█░░▄▀░░░░░░▄▀░░█░░░░▄▀▄▀▄▀░░░░█░░░░▄▀░░░░█░░▄▀░░░░▄▀▄▀░░█
                                █░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░███░░░░▄▀░░░░███░░▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀░░░░█
                                █░░░░░░░░░░░░░░█░░░░░░░░░░░░░░█████░░░░░░█████░░░░░░░░░░█░░░░░░░░░░░░███
██████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████
█░░░░░░░░░░░░███░░░░░░░░░░░░░░█░░░░░░░░░░░░░░█░░░░░░██░░░░░░█░░░░░░░░░░░░░░███░░░░░░░░░░░░░░█░░░░░░░░░░░░░░█░░░░░░░░░░░░░░░░███░░░░░░░░░░░░███
█░░▄▀▄▀▄▀▄▀░░░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░███░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀▄▀░░███░░▄▀▄▀▄▀▄▀░░░░█
█░░▄▀░░░░▄▀▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░░░░░█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░▄▀░░███░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░░░▄▀░░███░░▄▀░░░░▄▀▄▀░░█
█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░█████████░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░███░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░████░░▄▀░░███░░▄▀░░██░░▄▀░░█
█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░░░░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░▄▀░░░░█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░░░▄▀░░███░░▄▀░░██░░▄▀░░█
█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀▄▀░░███░░▄▀░░██░░▄▀░░█
█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░░░░░░░░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░░░░░▄▀░░░░███░░▄▀░░██░░▄▀░░█
█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░█████████░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░████░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░█████░░▄▀░░██░░▄▀░░█
█░░▄▀░░░░▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░░░░░░░░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░░░░░░░▄▀░░█░░▄▀░░░░░░▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀░░░░░░█░░▄▀░░░░▄▀▄▀░░█
█░░▄▀▄▀▄▀▄▀░░░░█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀▄▀▄▀▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀▄▀░░█░░▄▀░░██░░▄▀░░█░░▄▀░░██░░▄▀▄▀▄▀░░█░░▄▀▄▀▄▀▄▀░░░░█
█░░░░░░░░░░░░███░░░░░░██░░░░░░█░░░░░░░░░░░░░░█░░░░░░██░░░░░░█░░░░░░░░░░░░░░░░█░░░░░░░░░░░░░░█░░░░░░██░░░░░░█░░░░░░██░░░░░░░░░░█░░░░░░░░░░░░███
██████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████_|
#                                                                                `
console.log(consoleLogo)